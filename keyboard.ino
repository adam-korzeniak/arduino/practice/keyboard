#include <Keypad.h> //https://github.com/Chris--A/Keypad
 
const byte ROWS = 4;
const byte COLS = 4;
 
byte rowPins[ROWS] = {5, 4, 3, 2};
byte colPins[COLS] = {6, 7, 8, 9};
 
char keys[ROWS][COLS] = {
  {'*','0','#','D'},
  {'7','8','9','C'},
  {'4','5','6','B'},
  {'1','2','3','A'}
};
 
Keypad keyboard = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );
 
void setup(){
  Serial.begin(9600);  
}
  
void loop(){
  char key = keyboard.getKey();
  if (key) {
    Serial.println(key);
  }
}

boolean isPressed(char key) {
  return key == keyboard.getKey();
}
